object MainForm: TMainForm
  Left = 197
  Top = 111
  Width = 838
  Height = 521
  AlphaBlend = True
  AutoScroll = True
  Caption = 'SDI-Anwendung'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -19
  Font.Name = 'Segoe UI Light'
  Font.Style = []
  Menu = MainMenu
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 25
  object Label1: TLabel
    Left = 293
    Top = 8
    Width = 57
    Height = 25
    Caption = 'Name:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'Segoe UI Light'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lbl1: TLabel
    Left = 293
    Top = 39
    Width = 102
    Height = 25
    Caption = 'LongName:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'Segoe UI Light'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lLongName: TLabel
    Left = 405
    Top = 39
    Width = 93
    Height = 25
    Caption = 'lLongName'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'Segoe UI Light'
    Font.Style = []
    ParentFont = False
  end
  object lTypeName: TLabel
    Left = 408
    Top = 184
    Width = 90
    Height = 25
    Caption = 'lTypeName'
  end
  object Label2: TLabel
    Left = 293
    Top = 73
    Width = 48
    Height = 25
    Caption = 'Wert:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'Segoe UI Light'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label3: TLabel
    Left = 296
    Top = 184
    Width = 85
    Height = 25
    Caption = 'Typ Name:'
  end
  object StatusBar: TStatusBar
    Left = 0
    Top = 443
    Width = 822
    Height = 19
    Margins.Left = 2
    Margins.Top = 2
    Margins.Right = 2
    Margins.Bottom = 2
    AutoHint = True
    Panels = <>
    SimplePanel = True
  end
  object eName: TEdit
    Left = 405
    Top = 8
    Width = 209
    Height = 31
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
  end
  inline TreeFrame: TTreeFrame
    Left = 0
    Top = 0
    Width = 265
    Height = 443
    Align = alLeft
    AutoScroll = True
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    ExplicitWidth = 265
    ExplicitHeight = 443
    inherited Ueberschrift: TLabel
      Width = 255
      Height = 25
      Font.Height = -19
      Font.Name = 'Segoe UI Light'
      ExplicitWidth = 155
      ExplicitHeight = 25
    end
    inherited Tree: TTreeView
      Top = 38
      Width = 259
      Height = 402
      Font.Height = -19
      Font.Name = 'Segoe UI Light'
      ExplicitTop = 38
      ExplicitWidth = 259
      ExplicitHeight = 402
    end
  end
  object bStringCallEvent: TButton
    Left = 756
    Top = 70
    Width = 58
    Height = 33
    Caption = 'Call'
    TabOrder = 3
  end
  object eString: TEdit
    Left = 405
    Top = 70
    Width = 345
    Height = 33
    TabOrder = 4
    Text = 'eString'
  end
  object MainMenu: TMainMenu
    Left = 744
    object miDatei: TMenuItem
      Caption = 'Datei'
      object miNeu: TMenuItem
        Caption = 'Neu'
      end
      object N1: TMenuItem
        Caption = '-'
      end
      object miEnde: TMenuItem
        Caption = 'Ende'
        OnClick = miEndeClick
      end
    end
  end
end
