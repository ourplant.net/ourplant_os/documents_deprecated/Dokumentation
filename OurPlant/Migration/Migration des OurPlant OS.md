# Migration des OurPlant OS 2020

Ein brennende Frage stellt sich hinsichtlich der Migration der neuen Zellarchitektur in die bestehende OurPlant OS Software. Wir haben uns für eine "Grayfield" Lösung, also den Umbau der bestehenden Lösung entschieden. Das ist nicht ganz einfach, da im bestehendem System unterschiedlichste Lösungen aus unterschiedlichen Generationen und Technologien miteinander verwoben sind. Dennoch wäre das Aufsetzen eines absolut neuen Systems zeitlich wie kapazitätsmäßig kaum im Rahmen zu halten. 

Das Sample 1 aus dem Ordner Samples beschreibt die "Greenfield" Lösung. Das heißt wir setzen eine gänzlich neue Anwendung auf. Wir erinnern uns an die kleine SDI Anwendung, die ein Fenster mit der bestehenden Zellstruktur zeigt und diverse Änderungen und Anzeigen zulässt. 

![Sample 1 Bildschirm](Illustration/Sample 1 Bildschirm.png)

Der Einfachheit halber setzen wir auf dieser Anwendung auf und nutzen den einfachen Viewer, um unsere ersten Änderungen im OurPlant OS sichtbar und erlebbar zu machen. 

Voraussetzung für die Nutzung der Zellarchitektur ist natürlich erstmal die Einbindung der OurPlant Sources in das Projekt. Sie finden sich im Unterordner OurPlant. Dort sind für das folgende die Unterverzeichnis OurPlant/Common, OurPlant/Skillinterface und aus dem Unterordner OurPlant/Samples die FormViewerSample1 Dateien notwendig.

## Aktivierung der MicroCell

Durch das einbinden der benannten Ordner ist zwar die MicroCell Architektur in der Software bereits enthalten, sie ist aber noch nicht aktiv. Viele Objekte der bestehenden Software sind bereits MicroCells. Die MicroCell Architektur wurde quasi unter die bestehenden Objektstruktur untergeschoben. `TDestroyableObject` und `TSupervisedInterfacedObject` sind von `TCellObject` abgeleitet. Somit sind bereits alle Abkömmlinge eigentlich schon Zellen. Eigentlich deswegen, weil für eine wirkliche Nutzung noch zwei elementare Faktoren fehlen. Wie wir aus dem Workshop 2 - MicroCell Delphi Anwendung bereits gelernt haben brauchen wir zu aller erst eine gültige Zellenstruktur, die bei einem Discovery Manager beginnt. Weder ein Discovery Manager ist bislang aktiv, noch sind die Zellen auf irgend einem Pfad mit ihm verbunden. Das macht die "schlummernden" Zellen also schon mal ungültig. Was jedoch nicht stört, solange man ihre Zellfunktionalität nicht braucht bzw. verwendet. Zum zweiten sind die Zellen nicht registriert. Damit ist die derzeit niemand in der Lage, diese "Schläfer" aus dem MicroCell System heraus zu reproduzieren oder zuzuordnen. Mit dem Attribute `[RegisterCellType(Name,GUID)]` lässt sich das im weiteren Verlauf sehr einfach ändern.

Wir beginnen unsere Migration jedoch vorerst mit der Aktivierung des MicroCell Systems. Hierzu  binden wir in den Projektquelltext die Aktivierung des Discovery Managers ein.

```pascal
var
  vCmdLn: ICommandLineProcessor;
  I:      Integer;
  vEvent: TEvent;

begin
  Application.Initialize;

  TDesignManager.GetInstance;
  TUSBHookManager.GetInstance;
  TEurekaLogManager.GetInstance;

  // spring4d build container
  GlobalContainer.Build;

  if ParamCount > 0 then
  begin
    ExitCode := 0;
    vCmdLn := TCommandLineChecker.GetCommandLineChecker;
    try
      for I := 1 to ParamCount do
        vCmdLn.ProcessParameter(ParamStr(I));
      if vCmdLn.WasTestOnly then
      begin
        if not vCmdLn.TestResult then
          ExitCode := -1;
        Exit;
      end;
    finally
      vCmdLn := nil;
    end;
  end;

  Application.Title := 'OurPlantOS';
  FormatSettings := TFormatSettings.Create('de_DE');
  FormatSettings.DecimalSeparator := ',';
  FormatSettings.ThousandSeparator := '.';

  // Der Discovery Manager baut die Zellstruktur auf, durch das schreiben des Namen wird der DM gebaut (einmalig)
  TCellObject.RootSkill<IsiDiscoveryManager>.siDiscoveryName:= 'OurPlantOS';
  
  // durch Restore wird die letzte Struktur und Inhalte wiederhergestellt
  TCellObject.Root.siRestore;

  // der Style muss zuerst geladen werden
  TSys.DoCreateInit;

  vEvent := TEvent.Create(nil, False, False, '');
  try
    TOurPlantOSInitializer.DoInitialization(vEvent);

    while vEvent.WaitFor(100) <> wrSignaled do
      Application.ProcessMessages();

    TOurPlantOSInitializer.CleanUp;
  finally
    vEvent.Free;
  end;

  //ReportMemoryLeaksOnShutdown := True;
  Application.CreateForm(THaupt, Haupt);
  Application.Run;

  // Nach schließen der Anwendung sichert der Discovery Manager das µCell-System
  TCellObject.Root.siSave;

  try
    Haupt.Free;
  except

  end;

  // gelegentlich treffen USB Ereignisse ein, nachdem das Meiste schon heruntergefahren wurde
  TUSBHookManager.GetInstance.StopEvents;

  Application.ControlDestroyed(Haupt);

  sys.Close(TLoadscreenform.InitializeProgressScreen('Finalizing', 26));
end.
```

Durch das Schreiben von  `TCellObject.RootSkill<IsiDiscoveryManager>.siDiscoveryName:= 'OurPlantOS'` setzen wir den Namen des Systems unter dem der Discovery Manager die Root-Struktur der Zellen über den Data Manager lädt. Das erstmalige Ansprechen des Discovery Manager baut die Zellstruktur samts Discovery Manager als Default auf. Der anschließende Befehl `TCellObject.Root.siRestore`  lädt aus der Datei `OurPlantOS.json` die Struktur aus der letzten Sitzung (wenn eine Datei gefunden wird). Somit steht unser MicroCell System. Nach dem Schließen der Anwendung sichert `TCellObject.Root.siSave` die gesamte Zellstruktur in der Datei `OurPlantOS.json`. Einer Reproduktion der zukünftigen Systemdaten und Struktur steht ab sofort nichts mehr im Wege. Ganz gleich, was unser zukünftiges System an Veränderungen erfährt, diese zwei Änderungen im Main bleiben von nun an die eigentliche Konstante.

Kompilieren wir den Code und starten die Anwendung, dann sieht erstmal alles aus wie gehabt und wir freuen uns, dass wir wissen, dass im OurPlant OS jetzt eine MicroCell Struktur aktiviert ist. Sehen können wir es nur, wenn wir nach dem Schließen der Anwendung in den OurPlant\Data Verzeichnis nachschauen.

```json
{
  "cell":"root",
  "type.name":"Discovery manager",
  "type.guid":"{B2C66E5D-F763-4E09-A8EE-EC4ABFFEF706}",
  "value":"(empty)",
  "sub.cell.count":6,
  "constructor.cell.count":5,
  "sub.cells":
  [
        {
      "cell":"TypeRegister",
      "type.name":"Cell type register",
      "type.guid":"{D93364D5-C2AC-4DC4-B4CA-4BA22FDD59B9}"
    }
,
    
    {
      "cell":"PlaceHolderManager",
      "type.name":"Placeholder manager",
      "type.guid":"{045F27A6-6C1A-494D-BB48-14C71D89C877}"
    }
,
    
    {
      "cell":"StandardDataManager",
      "type.name":"Standard data manager",
      "type.guid":"{999FDEF4-BA2B-494A-87BF-085EA2E20538}",
      "value":"(empty)",
      "sub.cell.count":3,
      "constructor.cell.count":3,
      "sub.cells":
      [
                {
          "cell":"FileDir",
          "type.name":"String",
          "type.guid":"{21EEC5E5-9A52-497A-82AA-0C81314E6B44}",
          "value":"C:\\ourplant\\data"
        }
,
        
        {
          "cell":"siSaveCellJSONContent",
          "type.name":"Standard Cell",
          "type.guid":"{09EDF8FC-2638-47F4-9332-928396A4F4B7}"
        }
,
        
        {
          "cell":"siRestoreCellJSONContent",
          "type.name":"Standard Cell",
          "type.guid":"{09EDF8FC-2638-47F4-9332-928396A4F4B7}"
        }
      ]
    }
,
    
    {
      "cell":"siDataManager",
      "type.name":"Cell reference",
      "type.guid":"{7E64D16F-7942-4FB2-BA39-BFA2B59CD68D}",
      "value":"OP:/StandardDataManager"
    }
,
    
    {
      "cell":"siDiscoveryName",
      "type.name":"String",
      "type.guid":"{21EEC5E5-9A52-497A-82AA-0C81314E6B44}",
      "value":"OurPlantOS"
    }
  ]
}
```

 In der Datei OurPlant.json finden wir einen ersten Eindruck von der Zellstruktur. Was wir hier sehen, ist der Default und kleinste Nenner einer MicroCell Struktur. 

## Sichtbarkeit der Zellstruktur durch einfachen FormViewer

Da wir in unserer OurPlant OS Anwendung zukünftig etwas mehr Sichtbarkeit und Eingriffsmöglichkeiten in die neuen Struktur brauchen, implementieren wir uns als zweiten Schritt den einfachen FormViewer aus dem Sample 1 aus dem Workshop 1 Anwendung. Er kann sowohl als Hauptfenster, wie auch als Nebenfenster arbeiten.

```pascal
  // Der Discovery Manager baut die Zellstruktur auf, durch das schreiben des Namen wird der DM gebaut (einmalig)
  TCellObject.RootSkill<IsiDiscoveryManager>.siDiscoveryName:= 'OurPlantOS';
  
  // die App hat einen Form Viewer zur developer Kontroller
  TCellObject.Root.siConstructNewSubCell( 'Form Viewer Sample', 'DeveloperCellView');
  
  // durch Restore wird die letzte Struktur und Inhalte wiederhergestellt
  TCellObject.Root.siRestore;

  // der Style muss zuerst geladen werden
  TSys.DoCreateInit;

```

Nach dem Aufbau der MicroCell Struktur konstruieren (statisches addieren) wir in die Root (Discovery Manager) durch den Befehl  `TCellObject.Root.siConstructNewSubCell`.  Durch das Konstruieren (statt addieren) legen wir die Zelle statsich an und können sie somit später noch ändern bzw. entfernen, wenn wir sie so nicht mehr benötigen. Über die Verwendung des Type name rufen wir die im Zelltypregister abgelegte TcoFormViewerSample1 auf ohne auf die Unit direkt verweisen müssen (Unterbindung von Abhängigkeiten) . *Bemerkung*: Der spätere Restore wird nur addierte, aber nicht konstruierte Zellen wiederherstellen. Für die konstruierten Zellen werden nur die Inhalte wiederhergestellt. Somit können und müssen die konstruierten Zellen durch den Code rekonstruiert und auch verändert  werden.

Von nun an ist unser System um einen Viewer reicher. Damit wir uns den FormViewer auch zu nutzen machen können ergänzen wir in der Hauptseite (`THaup`) das Hauptmenü um einen Menüeintrag mit dem Shortcut `Strg+Alt+M` und rufen den Viewer über seine Skillmethode `siShowCell` auf.

```pascal
procedure THaupt.mniMicroCellFormViewerClick(Sender: TObject);
begin
  TCellObject.RootSkill<IsiFormViewer1>.siShowCell( TCellObject.Root );
end;
```

Starten wir die Anwendung erneut und drücken die Shortcuts Strg+Alt+M so erscheint der FormViewer aus der Sample 1 Anwendung im gewohnten Style des OurPlant OS

![FormViewer in OurPlant OS](Illustration/FormViewer in OurPlant OS.png)

Bis zur Zelle `siDiscoveryName` sehen wir den Default des Discovery Managers. Als letzte Zelle in der Struktur unter der Root sehen wir unseren gerade addierten `FormViewer`. Ab nun haben wir einen gutes Instrument, um uns die weiteren Ergebnisse unserer Migrationsarbeit einfach sichtbar zu machen und das MicroCell System gezielt zu beeinflussen.

Die eigentliche Migrationsarbeit kann nun beginnen.

## Wo fängt man an mit der Migration?

Wir bewegen uns "Up-down" von oben nach unten. Also fangen wir beim Objektquelltext an und arbeiten uns in das bestehende System hinein. 

Beim Blick auf den Objektquelltext sehen wir die Festlegung einiger Einstellungen. Das scheint ein recht einfach und dennoch wichtiger Eingriff zu sein. 

```pascal
  Application.Title := 'OurPlantOS';
  FormatSettings := TFormatSettings.Create('de_DE');
  FormatSettings.DecimalSeparator := ',';
  FormatSettings.ThousandSeparator := '.';
```

Wenn wir zukünftig diese Einstellungen aus dem Quelltext verbannen und zellisch organisieren wollen, müssen wir uns der Allgemeingültigkeit unserer neuen Architektur bewusst werden. Sicher machen alle diese Einstellungen für diese OurPlantOS Anwendung so Sinn. Ist das aber in anderen Anwendungen auch so. Eher nicht!  

> **Regel §1: Bei Migration muss die Allgemeingültigkeit des neuen Systems gewahrt bleiben.** Migration heißt: "Altes" in "Neues" überführen, aber dabei nicht "Altes" verschleppen.

Insofern müssen wir bei dem noch recht einfachen Code entscheiden, was davon hat allgemeingültigen Charakter und was nicht.

In Zukunft werden wir unterscheiden, ob wir bestehende Zellen und Skill-Interface um Fähigkeiten und Eigenschaften erweiteren oder ob wir neue Zellen gestalten. Insofern  bei der Migrationen aus "alten" Klassen noch alte Arithmetik oder Zugriffe benötigt werden, fügen wir die neuen Zellen vorerst in den Ordner OurPlant/Migration ein. Gleiches gilt auch für Zwischenstände von Skill-Interfaces.

## Erste kleinste Migration - der  `Application.Title` 

Der Applikationstitel (`Application.Title`) hat Potential für Allgemeinheit und schafft es als `siApplicationTitel` in den Discovery Manager. 

```Pascal
Application.Title := 'OurPlantOS';
```



Wir erweitern das Discovery Manager Interface um die Methoden & Eigenschaften von `siApplicationTitle`.

```pascal
  IsiDiscoveryManager = interface(IsiCellObject)
  ['{E3F35ABC-B03F-49ED-9CF9-C4AA47DA857D}']
    function siGetDataManager : IsiDataManager1;
    procedure siSetDataManager( const aDataManager : IsiDataManager1);
    property siDataManager : IsiDataManager1 read siGetDataManager write siSetDataManager;

    function siGetDiscoveryName : string;
    procedure siSetDiscoveryName( const aName : string);
    property siDiscoveryName : string read siGetDiscoveryName write siSetDiscoveryName;

    function siGetApplicationTitle : string;
    procedure siSetApplicationTitle( const aName : string);
    property siApplicationTitle : string read siGetApplicationTitle write siSetApplicationTitle;
  end;
```

 Die Implementierung der Methoden im TcoDiscoveryManager.





## Die erste "echte" Zelle - die FormatSetting Zelle



```pascal
 FormatSettings := TFormatSettings.Create('de_DE');
 FormatSettings.DecimalSeparator := ',';
 FormatSettings.ThousandSeparator := '.';
```



## PPSprache.pas - Den Abhängigkeiten auf der Spur



```pascal
unit PPSprache;

interface

uses
  OurPlant.Common.OurPlantObject,
  System.SyncObjs,
  Classes, 
  Windows, 
  Graphics, 
  SysUtils, 
  IniFiles, 
  grids, 
  controls, 
  menus, 
  extctrls, 
  System.Generics.Collections,
  common.SafeMemIni;

{ ******************* ÖFFENTLICHE KONSTANTEN **************************************** }
const
  SPRACH_EINTRAEGE     = 800;
  // SPRACH_DIR        ='C:\Arbeit\VicoBase.V15\Sprache';
  DEF_SPRACH_DIR       = 'Sprache';
  MAX_SPRACH_EINTRAEGE = 20000;
  CALC_TIME            = true;

  { *********************************************************************************** }
type
  INameTranslator = interface
    ['{28254F9E-11F8-4A09-A0F8-7002DC209C74}']
    function DefName: string;
    function GetName: string;
    property Name: string read GetName;
  end;

  TNameTranslator = class(TOurPlantObject, INameTranslator)
  private
    fTag:  integer;
    fName: string;
    function DefName: string;
    function GetName: string;

  public
    constructor Create(const ATag: integer; const AName: string);
    property Name: string read GetName;
  end;

  TTranslationData = record
    fTag: integer;
    fDefaultText, fStatictext: string;
    function Create(const ATag: integer; const ADefaultText, AStaticText: string): TTranslationData;
  end;

  // hält die Werte, die in der Sprachdatei fehlen, aber wegen der defaults angezeigt werden
  IItemsNotInList = interface
    ['{F1D13F9C-2D43-4A55-AE7A-F452C58FA6DE}']
    procedure AddTag(const ATag: Integer);
    function GetTag: Integer;
    procedure AddName(const AName: string);
    function GetName: string;

  end;

  TItemsNotInList=class(TOurPlantObject, IItemsNotInList)
  private
    fTag: integer;
    fName: string;

    // IItemsNotInList
    procedure AddTag(const ATag: Integer);
    function GetTag: Integer;
    procedure AddName(const AName: string);
    function GetName: string;
  end;

  TGenControl = class(TControl);

  TSprache = class(TObject)
  private
    fEintrag:          array [1 .. SPRACH_EINTRAEGE] of String;
    ItemList:          TStringList;
    fItemListDefaults: TList<IItemsNotInList>;
    fListNewTags:      TList<integer>;
    fCritSection:      TCriticalsection;
    fWasLoaded:        Boolean;

    function replaceNewLines(const aString: string): string;
    function GetItem(idx: LongInt): string;
    function GetCount: LongInt;

    procedure SetItem(idx: LongInt; value: string);
    procedure SetCount(value: LongInt);
    procedure StartPerformer(var AFrequency64, AStartTime64: int64);
    function EndPerformer(const AFrequency64, AStartTime64: int64;
      const AText: string): single;
    procedure PostTextMessage(const AMessage: string);

    class var _instance: TSprache;
    function GetLostItemCount: LongInt;
    function GetLostItemName(idx: Integer): String;
    function GetLostItemTag(idx: Integer): Integer;

  public
    { Public-Deklarationen }
    FileName:   String;
    Sprach_dir: string;
    DebugInfo:  Boolean;
    Modified:   Boolean;

    class function GetInstance: TSprache;
    constructor CreateInstance;
    destructor Destroy; Override;

    procedure LoadIniFile(fIni: TSafeIni);
    procedure SaveIniFile(fIni: TSafeIni);
    procedure LoadSprachFile;
    procedure SaveSprachFile;

    function ItemDef(idx: LongInt; Default: string): string;

    procedure ControlGenerator(twc: TComponent);
    procedure PopUpMenuGenerator(twc: TPopupMenu);
    procedure MenuItemGenerator(mi_parent: TMenuItem);
    procedure Translator(twc: TControl; idx: LongInt; DefaultText: String);
    procedure TranslatorExt(twc: TControl; idx: LongInt; DefaultText, ExtText: String);
    procedure PanelTranslatorExt(twc: TPanel; idx: LongInt; DefaultText, ExtText: String);
    procedure TranslateMenuItem(twc: TMenuItem; idx: LongInt; DefaultText: String);
    procedure TranslateMenuItemEx(twc: TMenuItem);

    procedure SListColTranslator1(twc: TStringGrid; Col, idx: LongInt; DefaultText: String);
    procedure SListColTranslatorN(twc: TStringGrid; Col, idx: LongInt; DefaultText: String);

    procedure SListRowTranslator1(twc: TStringGrid; Col, idx: LongInt; DefaultText: String);
    procedure SListRowTranslatorN(twc: TStringGrid; Col, idx: LongInt; DefaultText: String);

    { Eigenschaften }
    property Item[idx: LongInt]: String read GetItem write SetItem;
    property Count: LongInt read GetCount write SetCount;

    property LostItemTag[idx: LongInt]: Integer read GetLostItemTag;
    property LostItemName[idx: LongInt]: String read GetLostItemName;
    property LostItemCount: LongInt read GetLostItemCount;
  end;

function Sprache: TSprache;

```



### Die neue Unit-Struktur als erster Migrationsschritt

Die Unit `PPSprache.pas` werden wir <u>vorerst</u> erhalten. Sie wird in vielzählig (fast allen Unit verwendet). Fast alle Objekte greifen auf die Funktion `Sprache` zu. Genau diese Unit und Funktionsstruktur werden wir erhalten.

Grundsätzlich soll `TSprache` zu einer Zelle werden. Sie soll zukünftig nur noch aktiviert werden bis das letzte Objekt mit dieser Verwendung verschwunden ist. Bis dahin soll sie unter root (Discovery Manager) angeordnet werden und direkt über die Funktion `Sprache` angesprochen werden.

Parallel zur `PPSprache.pas` entsteht im Migrationsbereich der MicroCell Struktur die Unit `OurPlant.Migration.Sprache.pas` für neue Skillinterface (mi's) und Zellobjekt (und auch diversen Hilfsobjekten, Typen und Konstanten) .

### `TSprache` , seine Hilfsklassen und Interfaces ziehen um

```pascal
unit OurPlant.Migration.Sprache.Cell;

interface

uses
  OurPlant.Common.OurPlantObject,
  OurPlant.Common.CellObject,
  common.SafeMemIni, //IniFiles,
  System.Generics.Collections, System.SyncObjs, Classes, Windows, SysUtils,  grids, controls, menus, extctrls;

const
  SPRACH_EINTRAEGE = 800;
  DEF_SPRACH_DIR       = 'Sprache';
  MAX_SPRACH_EINTRAEGE = 20000;
  CALC_TIME = true;

type
  IItemsNotInList = interface

  TItemsNotInList=class(TOurPlantObject, IItemsNotInList)

  TGenControl = class(TControl);

  TSprache = class(TObject)

```



### Aufräumen, Ordnen und diverse Konflikte auflösen

#### Die CALC_TIME Konstante

`CALC_TIME` von `ProgrammParameter.pas` nutzt `CALC_TIME` von `PPSprache.pas`.

```pascal
procedure TProP.StartPerformer(var AFrequency64, AStartTime64: int64);
begin
  AFrequency64 := 0;
  AStartTime64 := 0;

  if CALC_TIME then
  begin
    QueryPerformanceFrequency(AFrequency64);
    QueryPerformanceCounter(AStartTime64);
  end;
end;
```

--> wir lassen vorerst diese Konstante noch in `PPSprache` --> **muss später anders geregelt werden!**

#### Doppelte Sprache Funktion in SysTypes 

`Libraries.SysTypes` definiert nochmal die Funktion Sprache und reicht den Aufruf aus `PPSprache` nur durch.

```pascal
function Sprache: TSprache;
begin
  Result := PPSprache.Sprache;
end;
```

--> wir lassen die Funktion noch in `SysTypes`, ergänzen den Use Bereich um `OurPlant.Migration.Sprache` --> die Auflösung des Funktionsaufrufs bringt einige Auswirkungen zum Vorschein.

Auswirkungen und Änderungen in den Units : 

```
libraries.Mathe , components.interfaces, common.gui.frmListView, VWLGD302011Material, machinedata.TimedEventsProvider, OurPlant.CELLMigration.SKILLInterface.Generic.Dummy, vicosystem.externalEvaluation.NeuroCheck, machinedata.DataProvider, substrate.fileimport.OSRAMKombinationsDatei, VW3Gurtfeeder, VWWerkzeugdetektion, VWWerkzeugwechsler2, VWLGD2K, VWInspektion, VWInspektPosition, VWWaferPickUpKonfig, VW3DAufnahme, VWKopfKamera2009, VWDirektdosierer, position.ParallelBase, position.HeliotisInspectionFrame, VWZKU, VWLGD, VWWaferPickUp, VWHeizen, VWKopfKonsole, VWNadelkalibrierung, VWHotSpot, VWStauTS2, VWTransportIO2, VWTransportIO1, VWNadelReinigung, VWEWW, VW3ZConveyor, VW3x3Heizung, VWDeviceNet, VWWaage, VWUniHot, VWScanner, VWStauTS, VWExtSub, VWGurtfeeder, VWUniTS, VW2ServoAchsen, VWLGDVSH, VWHorizontalinspektion, VWBestueckkopf2009, VWUNIGPH, VWIO16, VWIO32, VWVakuumHot, VWBeleuchtung, VWAIO1, VWAnalogAusgang10, VWWerkzeugwechslermulti, VWLaser, VWWaferPickUpProgramm, VWPortalPosition, VWFloodUV, VWKopfKonsole2011, VWMiniDrive, VW3DBK2011, VWVorbestueckung, VW3DZKU, VWLGD2011, VWUniversalKopf2011, VW2DBK2011, VWServoAchse, VWFlipStation, VWManipulatorEinheitKalibrierung, VW3DSupport, VWUniversalPosition, VWExtInspektion, VWSchublade, VWKlappe, VWServostar300, VWServostar400, VWVakuumHubKlem, VWKopf, VWBestueckKopfKom, VWUniversalKlemmung, VWCANKomponente, VWEWE, VWJumoBox, VWConveyor2013Konfiguration, VWSensor, VWSchrittMotorManager, VWConveyor2013, VWHoehenMessung, VWLeitrechnerMDA, VWLGDJ30, VWStauTS2Zusatz, VWFritschFeeder, VWTrackball, VWEcovario, components.scanner.FakeScannerFrame, components.signallightFrame, components.communication.StandardComportFrame, components.communication.TerminalFrame, components.communication.CANComportFrame, position.frmHeadPosition, components.needlecleaning.DippingBathFrame, components.support.VacuumSupportFrame, position.CategorizationFrame, VWScanPosition, components.inspection.KeyenceInspectionUnitFrame, communication.tcpconnection.TCPServer, communication.tcpconnection.TCPConnection, position.frmSpecialConfiguration, components.partpreparation.frmPinCutter, components.video.SignalSwitcherFrame, components.video.ObservationChannelProviderFrame, components.heads.tools.frmCalibrationPlate, components.needlecleaning.JetCleaningUnitFrame, position.TriggerPositionFrame, components.heads.laser.frmLaserHead, components.heads.bonding.FineTechIOModuleFrame, components.axis.DesktopGantryFrame, components.ConfigurationFrame, components.heads.placing.BondHeadFrame, components.heads.bondingus40.BondHeadUS40Frame, components.chassis.ManualDrawerFrame, components.chassis.ServiceTimeSwitchFrame, components.heads.dispensing.frmTimePressureDispenser, components.support.AutomaticLoaderFrame, components.transport.frmManualDEU, components.heads.dispensing.MarcoJetDispenserFrame, components.communication.CommunicationModuleValueFrame, part.magazine.CountingMagazine, communication.frmDummyModuleConfig, components.axis.AAxisFrame, components.transport.tailbackconveyor.frmTailbackConveyor, components.plasma.RelyOnFrame,  components.iomodules.AlliedVisionClampingLightsFrame, components.safety.CoverFrame, components.plasma.FlowControllerFrame, components.partpreparation.flipstation.SwivelGripperFrame, components.chassis.frmShot, components.screwdriver.XPAQASGFrame, components.specialpickup.PressingStationFrame, components.uvsource.UVPanasonicFrame, components.directdispensing.FluxerFrame, components.transport.conveyorpressing.DreiZonenConveyorPressingFrame, components.transport.conveyorpressing.DreiZonenConveyorPressingControllerFrame, components.heads.dispensing.frmVermesDispenser, VWNanotecCL3, interfaces.AlignmentLogic, communication.protocols.SchusterDataAhead, positions.DesolderingFrame, DesolderingDrawPositionFrame, position.frmReceiveProductResults, position.ResistanceMeasurePositionFrame, position.ResistanceMeasurePositionUniFrame, components.heads.aoi.frmWickon, components.heating.miniheating.MiniHeatingFrame, components.transport.Conveyor1ZoneExternXRay.Conveyor1ZoneExtXRayFrame, components.axis.FaulhaberMcdc3002SFrame, interfaces.components.ServoAxisFrame, interfaces.components.TapeStampingUnitFrame,  components.transport.frmStackingMagazine, part.magazine.StackingMagazine, components.heating.idcheating.IDCHeatingFrame, OurPlant.CELLMigration.Conveyor.coConveyor3ZonesTactFrame, OurPlant.CELLMigration.Conveyor.coConveyor2Zones2StopperFrame, OurPlant.CELLMigration.Conveyor.coConveyor2Zones2StopperConfiguration, communication.tcpconnection.frmTCPServer, components.dotnetservice.ResistanceMeasuringFrame, components.dotnetservice.ResistanceMeasuringUniFrame, common.gui.IsiDigitalOutput, common.gui.IsiAnalogInputV0, components.support.wafertool.frmOneCircuit, components.support.WaferTransferTool, components.transport.feeder.JTF3, components.transport.feeder.frmJTF3, components.transport.feeder.JTF2, components.transport.feeder.frmJTF2, components.support.IntermediateSupport,  components.axis.SmarpodFrame, VWLAMLensePlacerFrame, components.ModuleGroups, part.Magazine.frmRemotePickPositionMagazine, part.Magazine.RemotePickPositionMagazine, components.transport.smartsight.SmartSightFrame, components.transport.smartsight.SmartSightDevelopFrame, components.axis.SmarActFrame, components.cameras.supplies.NDFilterFrame, communication.protocols.LaserPowerSupply, components.LaserDiodePowerSupply, components.heads.contacting.ContactUnitFrame, components.sensors.PowermeterFrame, position.placement.ActiveAlignmentLAM, components.alignment.AlignmentLogicModule, components.MicroService.SmarActLAMFrame, components.axis.Shuttle, components.axis.frmShuttle, components.axis.JennyScienceXenax, components.heads.dispensing.frmTimePressureDispenserV2, components.iomodules.Can2IO, components.sensors.PyrometerFrame, components.transport.Conveyor1ZoneExtern.Conveyor1ZoneExtFrame, components.transport.Conveyor1ZoneExtern.Conveyor1ZoneExtWidthFrame, components.transport.montratec.MontratecControlFrame, components.transport.montratec.MontratecPropertiesFrame, components.port.USBObserverFrame, components.transport.DreiZonenConveyor.RGWidthFrame, VWUVQuelle, VWUVQuelleHoenle, VWUVQuelleOmnicure, OurPlant.CELLMigration.LogicModule.IDCSinterPressModule, OurPlant.CELLMigration.LogicModule.IDCSinterPressConnectionFrame, OurPlant.CELLMigration.LogicModule.IDCSinterPressModuleV2, OurPlant.CELLMigration.LogicModule.IDCSinterPressConnectionFrameV2, OurPlant.CELLMigration.LogicModule.IDCSinterPressHeatingFrame, OurPlant.CELLMigration.LogicModule.IDCSinterPressHeatingFrameV2, OurPlant.CELLMigration.LogicModule.IDCSinterPressLoggingFrame, OurPlant.CELLMigration.LogicModule.IDCSinterPressLoggingFrameV2, OurPlant.CELLMigration.Periphery.Module242Frame, OurPlant.CELLMigration.Periphery.Module446Frame, OurPlant.CELLMigration.Periphery.Module461Frame, OurPlant.CELLMigration.Periphery.Module465Frame, OurPlant.CELLMigration.Periphery.Module474Frame, OurPlant.CELLMigration.ProcessPositions.coSinterContactPositionFrame, OurPlant.CELLMigration.ProcessPositions.coSinterContactPositionFrameV2, position.dataproviders.DynamicProviderPosition, position.dataproviders.frmDynamicProviderPosition, position.dataproviders.RequestData, position.dataproviders.frmRequestData, position.dataproviders.RequestDataV2, position.dataproviders.frmRequestDataV2, position.CategorizationPartCorrectionFrame, position.CheckAlignmentLAM, position.MeasurementCheckParallel, components.transport.conveyor.OPConveyorFrame
```

muss in `use` (vorzugsweise im Implementierungsteil) nun vorerst die unit `PPSprache` aufgenommen werden. Diese Uses können mögliche Probleme für den Compiler als Zirkulation mit sich bringen. Die Auflösung der Sprachvereinbarung reduziert die Zirkulation jedoch nur um einen Teilnehmer. Hier wird es noch wichtig sein, dass `PPSprache` zukünftig kaum Abhängigkeiten in den OurPlant Code hinein hat und die zugrundeliegende Zellarchitektur nicht auf `PPSprache` aufbaut.

#### Private Lösung in `TVersionInformation`

`TVersionInformation` in `common.VersionInformation` baut sich eine eigene privat Variable `fSprache : TSprache`.

```pascal
  TVersionInformation = class( TSupervisedInterfacedObject, IVersionInformation )
  strict private
  var
    ...
    fSprache:             TSprache;
    ...
```

Diese Variable löschen wir und ersetzen die Verwendung von `fSprache` gegen den ab jetzt für alle allgemeingültigen Aufruf `Sprache` aus `PPSprache.pas`.

Die "neue" Lösung `INameTranslator` und `TNameTranslator` wird nur einmal durch `TChassisProcessManager` verwendet. Sie wird zur Vorverdichtung in die Unit `machinesystem.ChassisProcesses.Manager` verlagert. Die einzigen Aufruf kommen aus `machinesystem.ChassisProcesses.PanelProcesses`. Die werden wir zu einem späteren Zeitpunkt räumen. 

```pascal
  INameTranslator = interface
    ['{28254F9E-11F8-4A09-A0F8-7002DC209C74}']
    function DefName: string;
    function GetName: string;
    property Name: string read GetName;
  end;

  TNameTranslator = class(TOurPlantObject, INameTranslator)
  private
    fTag:  integer;
    fName: string;
    function DefName: string;
    function GetName: string;
  public
    constructor Create(const ATag: integer; const AName: string);
    property Name: string read GetName;
  end;
```

#### Nicht genutzten Code aufräumen

Den Recordtyp `TTranslationData`  verwendet niemand. 

```pascal
  TTranslationData = record
    fTag: integer;
    fDefaultText, fStatictext: string;
    function Create(const ATag: integer; const ADefaultText, AStaticText: string): TTranslationData;
  end;
```

Diesen geben wir in diesem Zuge auf. Er entfällt ersatzlos.

#### Das Resultat ist schon überschaubarer

Mittlerweile bleit in der PPSprache.pas nur noch eine kleine überschaubare Code Menge übrig.

```pascal
unit PPSprache;

interface

uses
  OurPlant.Migration.Sprache.Cell;

const
  CALC_TIME = true; // muss noch bereinigt werden, zum Beispiel in einen allgemeine Verwaltung überführt werden

  // Sprache ist für die bestehende Architektur ab JETZT der allgemeine Zugriff
  // auf die herkömmliche Sprache Verwaltung, soll von überall genutzt werden
  // Diese Funktion wirkt unabhängig ihrer zukünftigen internen Umsetzung (als Zelle)
  function Sprache: TSprache;

```

> WICHTIG: Die `Sprache` Funktion von `PPSprache.pas` wird ab sofort in der bestehenden Architektur als allgemeingültiger Aufruf für die Sprachverwaltung nach herkömmlicher Art verwendet. Was hinter der Funktion implementiert wird, muss für die zukünftige Verwendung nicht mehr relevant sein.

Noch ist `Sprache` vom Typ Objektreferenz `TSprache`. Dies wird sich im Zuge der Migration noch ändern. 

`INameTranslator` und `TNameTranslator` wird nur zweimal als eine weitere Variante von Sprache in `machinesystem.ChassisProcesses.Manager` verwendet. Sie macht jedoch nicht sehr viel Neues, außer als Interface referenziert zu werden. Diese Variante werden wir später durch die Referenzierung auf die Zellen via Skillinterface ersetzen. Vorerst bleibt sie aber noch.

### Aus `TSprache` wird die Zelle mit dem Migrationsinterface `ImiLanguage`

#### Schritt 1: Ordnen und Vorbereiten des Public Bereiches

Im ersten Schritt bereiten wir den öffentlichen Bereich für die Überführung in die Interface Technologie vor. Hier unterscheiden wir in zukünftige Interface Methoden, die sich dann im Migrationsinterface wiederfinden und den noch benötigten öffentlichen Methoden zur Objektsteuerung. Einige der öffentlichen Methoden werden in diesem Zusammenhang entfallen bzw. in die dafür vorbereiteten Zellmethoden überführt.

```pascal
  public
    { Public-Deklarationen }
    FileName:   String;
    Sprach_dir: string;
    DebugInfo:  Boolean;
    Modified:   Boolean;

    class function GetInstance: TSprache;
    constructor CreateInstance;
    destructor Destroy; Override;

    procedure LoadIniFile(fIni: TSafeIni);
    procedure SaveIniFile(fIni: TSafeIni);
    procedure LoadSprachFile;
    procedure SaveSprachFile;

    function ItemDef(idx: LongInt; Default: string): string;

    procedure ControlGenerator(twc: TComponent);
    procedure PopUpMenuGenerator(twc: TPopupMenu);
    procedure MenuItemGenerator(mi_parent: TMenuItem);
    procedure Translator(twc: TControl; idx: LongInt; DefaultText: String);
    procedure TranslatorExt(twc: TControl; idx: LongInt; DefaultText, ExtText: String);
    procedure PanelTranslatorExt(twc: TPanel; idx: LongInt; DefaultText, ExtText: String);
    procedure TranslateMenuItem(twc: TMenuItem; idx: LongInt; DefaultText: String);
    procedure TranslateMenuItemEx(twc: TMenuItem);

    procedure SListColTranslator1(twc: TStringGrid; Col, idx: LongInt; DefaultText: String);
    procedure SListColTranslatorN(twc: TStringGrid; Col, idx: LongInt; DefaultText: String);

    procedure SListRowTranslator1(twc: TStringGrid; Col, idx: LongInt; DefaultText: String);
    procedure SListRowTranslatorN(twc: TStringGrid; Col, idx: LongInt; DefaultText: String);

    { Eigenschaften }
    property Item[idx: LongInt]: String read GetItem write SetItem;
    property Count: LongInt read GetCount write SetCount;

    property LostItemTag[idx: LongInt]: Integer read GetLostItemTag;
    property LostItemName[idx: LongInt]: String read GetLostItemName;
    property LostItemCount: LongInt read GetLostItemCount;
  end;

```

Als Erstes bauen wir die Publik-Variablen um. (Variablen sind im Interface nicht zulässig).

Aus  den Variablen

```pascal
    FileName:   String;
    Sprach_dir: string;
    DebugInfo:  Boolean;
    Modified:   Boolean;
```

werden Getter und Setter-Methoden und Properties.

```pascal
    function GetFileName : string;
    procedure SetFileName( const aValue : string);
    property FileName : string read GetFileName write SetFileName;

    function GetSprachDirectory : string;
    procedure SetSprachDirectory( const aValue : string);
    property SprachDirectory : string read GetSprachDirectory write SetSprachDirectory;

    function GetDebugInfo : Boolean;
    procedure SetDebugInfo( const aValue : Boolean);
    property DebugInfo : Boolean read GetDebugInfo write SetDebugInfo;

    function GetModified : Boolean;
    procedure SetModified( const aValue : Boolean);
    property Modified : Boolean read GetModified write SetModified;
```

Wir mögen vielleicht geneigt sein, im Code gleich zu optimieren. Insbesondere bei `SprachDirectory` fallen da gleich einige Dinge auf. Codeoptimierung werden wir aber auf später verschieben. Notwendige Änderungen wie var Zuweisungen werden wir jedoch zwangsläufig schon jetzt korrigieren. Den Setter von SprachDirectory können wir weg lassen, da außerhalb niemand die Eigenschaft schreibt.

Die anderen Methoden (mit Ausnahme Methoden der Objektsteuerung) sind soweit OK. Die Getter der Properties holen wir aber der Ordnung halber mit zu den Property Einträgen.

```pascal
    function  GetItem(idx: LongInt): string;
    procedure SetItem(idx: LongInt; value: string);
    property  Item[idx: LongInt]: String read GetItem write SetItem;

    function  GetCount: LongInt;
    procedure SetCount(value: LongInt);
    property  Count: LongInt read GetCount write SetCount;

    function GetLostItemTag(idx: Integer): Integer;
    property LostItemTag[idx: LongInt]: Integer read GetLostItemTag;

    function GetLostItemName(idx: Integer): String;
    property LostItemName[idx: LongInt]: String read GetLostItemName;

    function GetLostItemCount: LongInt;
    property LostItemCount: LongInt read GetLostItemCount;
```

#### Schritt 2: `TSprache` wird zur Zelle `TmoLanguage`

Aus `TSprache` wird `TmoLanguage`. "Tmo" steht dabei für "Type migration object". Es wird bereits registriert unter "PPSprache".

```pascal
  [RegisterCellType('PPSprache', '{3A59E43A-63C2-4FA3-BAD1-9BF1B97C617C}' )]
  TmoLanguage = class(TCellObject)
```

Das "neue" Objekt ist schon eine registrierte Zelle. Sie wird aber noch "althergebracht" als Objekt in der PPSprache.pas gebaut und angesprochen.

```pascal
function Sprache: TmoLanguage;
begin
  result := TmoLanguage.GetInstance;
end;
```

#### Schritt 3: Aufbau des Migrationsinterface `ImiLanguage`

```pascal
unit OurPlant.Migration.Sprache.SkillInterface;

interface

uses
  OurPlant.Common.CellObject,
  common.SafeMemIni, //IniFiles,
  System.Classes,
  Vcl.Menus,
  Vcl.Controls,
  Vcl.ExtCtrls,
  Vcl.Grids;

type
  ImiSprache = interface(IsiCellObject)
    ['{6C515CB8-29CA-4196-8478-905CBEA6B58A}']

    procedure LoadIniFile(fIni: TSafeIni);
    procedure SaveIniFile(fIni: TSafeIni);

    procedure LoadSprachFile;
    procedure SaveSprachFile;

    function ItemDef(idx: LongInt; Default: string): string;

    procedure ControlGenerator(twc: TComponent);
    procedure PopUpMenuGenerator(twc: TPopupMenu);
    procedure MenuItemGenerator(mi_parent: TMenuItem);
    procedure Translator(twc: TControl; idx: LongInt; DefaultText: String);
    procedure TranslatorExt(twc: TControl; idx: LongInt; DefaultText, ExtText: String);
    procedure PanelTranslatorExt(twc: TPanel; idx: LongInt; DefaultText, ExtText: String);
    procedure TranslateMenuItem(twc: TMenuItem; idx: LongInt; DefaultText: String);
    procedure TranslateMenuItemEx(twc: TMenuItem);

    procedure SListColTranslator1(twc: TStringGrid; Col, idx: LongInt; DefaultText: String);
    procedure SListColTranslatorN(twc: TStringGrid; Col, idx: LongInt; DefaultText: String);

    procedure SListRowTranslator1(twc: TStringGrid; Col, idx: LongInt; DefaultText: String);
    procedure SListRowTranslatorN(twc: TStringGrid; Col, idx: LongInt; DefaultText: String);

    function  GetFileName : string;
    procedure SetFileName( const aValue : string);
    property  FileName : string read GetFileName write SetFileName;

    function GetSprachDirectory : string;
    property SprachDirectory : string read GetSprachDirectory;

    function  GetDebugInfo : Boolean;
    procedure SetDebugInfo( const aValue : Boolean);
    property  DebugInfo : Boolean read GetDebugInfo write SetDebugInfo;

    function  GetModified : Boolean;
    procedure SetModified( const aValue : Boolean);
    property  Modified : Boolean read GetModified write SetModified;

    function  GetItem(idx: LongInt): string;
    procedure SetItem(idx: LongInt; value: string);
    property  Item[idx: LongInt]: String read GetItem write SetItem;

    function  GetCount: LongInt;
    procedure SetCount(value: LongInt);
    property  Count: LongInt read GetCount write SetCount;

    function GetLostItemTag(idx: Integer): Integer;
    property LostItemTag[idx: LongInt]: Integer read GetLostItemTag;

    function GetLostItemName(idx: Integer): String;
    property LostItemName[idx: LongInt]: String read GetLostItemName;

    function GetLostItemCount: LongInt;
    property LostItemCount: LongInt read GetLostItemCount;
  end;
```

Durch die Zuweisung von `TmoLanguage = class(TCellObject, ImiSprache )` wird die "neue" Zelle mit dem neuen Migrationsinterface ausgestattet. Das Migrationsinterface trägt jetzt sowohl die Zellarchitektur (via IsiCellObject) als auch alle bisherigen Methoden / Fähigkeiten von `TSprache` nach außen. 

#### Schritt 4: Die Steuerung der neuen Zelle anpassen

Die Klassenvariable `class var _instance: TmoLanguage` und die Klassenfunktion `class function GetInstance: TmoLanguage` entfallen. Zukünftig wird die Referenz der Zelle als Subzelle im Discovery Manager verwaltet oder als globale Referenzvariable (als Cache). vorgehalten.

Aus dem Konstruktor `CreateInstance` wird die Prozedur `CellConstruction` (`override`).

```pascal
procedure TmoLanguage.CellConstruction;
var
  i: integer;
begin
  inherited;

  fCritSection := TCriticalsection.Create;

  ItemList := TStringList.Create;
  for i := 1 to SPRACH_EINTRAEGE do
    fEintrag[i] := '';

  fListNewTags := TList<integer>.Create;
  fItemListDefaults:= TList<IItemsNotInList>.create;

  FileName := ExtractFilePath(Application.EXEName) + DEF_SPRACH_DIR + '\' + DEFAULT_FileName;
  fSprachDirectory := ExtractFilePath(Application.EXEName) + DEF_SPRACH_DIR;
  fDebugInfo := DEFAULT_DebugInfo;
  fModified := false;
end;
```

Aus dem Destruktor `Destroy` wird die Prozedur `BeforeDestruction` (`override`).

```pascal
procedure TmoLanguage.BeforeDestruction;
begin
  fCritSection.Free;
  fListNewTags.Free;
  ItemList.Free;
  fItemListDefaults.Free;

  inherited;
end;
```

Aus dem `destructor TSys.Close(AStatusStepper: IProgressStepper)` 

```pascal
  vStatusStepper.NextStep('Free ... Language');
  PostTextMessage('Free ... Language');
  Sprache.Free;
```

muss die Freigabe entfernt werden. Die Zelle wird jetzt durch den Discovery Manager gesteuert und auch freigegeben.

#### Schritt 5: Anpassung des Zellaufrufs in der `Sprache` Funktion

Der Aufruf der Sprachzelle wird zukünftig ausschließlich über das neue Migrationsinterface stattfinden.

```pascal
function Sprache: ImiSprache;
```

Bei der Referenzierung wird gleich mit auf die Existenz und Gültigkeit der Zelle reagiert. Hierbei gibt es zwei Varianten.

**Variante 1**: Die Referenzierung findet immer dynamisch über den Discovery Manager statt.

```pascal
uses
  OurPlant.Common.CellObject;

function Sprache: ImiSprache;
begin
  if not TCellObject.TryRootSkill<ImiSprache>(Result) and
   not TCellObject.Root.siConstructNewCell(TmoLanguage,'PPSprache',ImiSprache,@Result) then
    raise('Unvalid creation of PPSprache migration cell in root!')
end;
```

Über die Klassenfunktion von TCellObject versucht die Funktion erstmal eine Referenz von ImiSprache unter Root zu finden. Gelingt das nicht, versucht sie eine Zelle vom Typ `TmoLaguage` (mit `ImiSprache`) in die root zu konstruieren. Die Zelle darf nicht addiert, sondern muss konstruiert werden, damit sie statisch angelegt wird. Erstens räumt Restore mit allen addierten dynamischen Zellen, Sprache würde dann immer wieder neu angelegt werden müssen. Zweitens verhindert der statische konstruktive Aufbau, dass die Sprache Zelle für immer und ewig gebaut wird, obwohl sie irgendwann mal nicht mehr gebraucht wird.

**Variante 2**: Die Referenzierung findet gepuffert über eine interne Variable statt.

```pascal
var
  VSpracheRef : ImiSprache;

function Sprache: ImiSprache;
begin
  // überprüft die Gültigket der Variable VSpracheRef als Referenz auf ImiSprache in root
  if TCellObject.IsValidAs<ImiSprache>(VSpracheRef) then
  begin
    Result := VSpracheRef;
  end
  else
  begin
       // versucht eine Zelle mit ImiSprache Interface unter root zu finden
    if TCellObject.TryRootSkill<ImiSprache>(Result) or
       // versucht ansonsten eine TmoLanguage Zelle mit ImiSprache in der root anzulegen
       TCellObject.Root.siConstructNewCell(TmoLanguage,'PPSprache',ImiSprache,@Result) then
    begin
      VSpracheRef := Result
    end
    else
    begin
      result := nil;
      raise Exception.Create( 'No valid PPSprache cell in root!');
    end;
  end;
end;
```

Da auf die Sprache Zelle sehr häufig (eigentlich permanent) zugegriffen wird, bietet sich eine derartige Pufferung an, um Rechenzeit zu sparen. Die Performanz kann noch etwas gesteigert werden, indem nicht die Zellgültigkeit geprüft wird, sondern nur das Assign getestet wird. Da diese Zelle ausschließlich hier und statisch gebaut wird, kann man diesen Kompromiss durchaus in Erwägung ziehen.

```pascal
  if Assigned(VSpracheRef) then
    Result := VSpracheRef
  else
    ...
```

In dieser Puffer Variant muss am Ende der Nutzung die Referenz auf die Zelle gelöscht werden, ansonsten wird das Objekt von `TmoLanguage` (ehemals `TSprache`) nicht zerstört. (Memory Leak)

```pascal
finalization
  VSpracheRef := nil;
```

### Die neue Unit `OurPlant.Migration.Common.GlobalCellAccess`

Der Rest von `PPSprache` wandert in eine neue Unit `OurPlant.Migration.Common.GlobalCellAccess`, die wir zukünftig als Brücke globaler Zugriffe aus dem gesamten "alten" System zu den neuen Zellen benötigen. 

```pascal
unit OurPlant.Migration.Common.GlobalCellAccess;

interface

uses
  OurPlant.Migration.Sprache.SkillInterface;
const
  CALC_TIME = true; // muss noch bereinigt werden, zum Beispiel in einen allgemeine Parameter-Verwaltung überführt werden

  /// <summary>
  ///   Get a valid reference of migration skill interface ImiSprache. If cell
  ///   not exist in root, then construct it.
  /// </summary>
  function Sprache : ImiSprache;

implementation

uses
  OurPlant.Migration.Sprache.Cell,
  OurPlant.Common.CellObject,
  System.SysUtils;

var
  VSpracheRef : ImiSprache;

function Sprache: ImiSprache;
begin
  // überprüft die Gültigket der Variable VSpracheRef als Referenz auf ImiSprache in root
  //if TCellObject.IsValidAs<ImiSprache>(VSpracheRef) then
  if Assigned(VSpracheRef) then
    Result := VSpracheRef
  else
  begin
       // versucht eine Zelle mit ImiSprache Interface unter root zu finden
    if TCellObject.TryRootSkill<ImiSprache>(Result) or
       // versucht ansonsten eine TmoLanguage Zelle mit ImiSprache in der root anzulegen
       TCellObject.Root.siConstructNewCell(TmoLanguage,'Sprache',ImiSprache,@Result) then
    begin
      VSpracheRef := Result
    end
    else
    begin
      result := nil;
      raise Exception.Create( 'No valid OurPlant.Migration.Common.GlobalCellAccess cell in root!');
    end;
  end;
end;

initialization

finalization
  VSpracheRef := nil;

end.
```

Da so ziemlich in allen Units `PPSprache` zu finden ist, nutzen wir das externe Tool `Notepad++` zum Ersetzen alle Einträge von `PPSprache` zum ersetzen auf `OurPlant.Migration.Common.GlobalCellAccess`. Dazu wähle im Menü `Suchen` den Punkt `In Dateien suchen`.

<img src="Illustration/NotepadPlusInDateienSuchen.png" alt="NotepadPlusInDateienSuchen" style="zoom:150%;" />



### Völlig losgelöst

Wir können die Abhängigkeiten der GlobalCellAcces Unit zur Sprach-Unit (jetzt OurPlant.Migration.Sprache.Cell) mit seinen ganzen genutzten Units aus der bestehenden Architektur unterbinden, indem wir die Zelle nicht direkt aus TmoLanguage, sondern im CellTypeRegister über den Typnamen von der SprachZelle  bauen.

```pascal
function Sprache: ImiSprache;
begin
  // überprüft die Gültigket der Variable VSpracheRef als Referenz auf ImiSprache in root
  //if TCellObject.IsValidAs<ImiSprache>(VSpracheRef) then
  if Assigned(VSpracheRef) then
    Result := VSpracheRef
  else
  begin
       // versucht eine Zelle mit ImiSprache Interface unter root zu finden
    if TCellObject.TryRootSkill<ImiSprache>(Result) or
       // versucht ansonsten eine TmoLanguage Zelle mit ImiSprache in der root anzulegen
       TCellObject.Root.siConstructNewCell('MigrationSprache','Sprache',ImiSprache,@Result) then
    begin
      VSpracheRef := Result
    end
    else
    begin
      result := nil;
      raise Exception.Create( 'No valid OurPlant.Migration.Common.GlobalCellAccess cell in root!');
    end;
  end;
end;
```

damit können wir  `OurPlant.Migration.Sprache.Cell` aus dem use Block entfernen und haben nun nur noch die Abhängigkeit zur Interface `unit OurPlant.Migration.Sprache.SkillInterface`, die selber keine Anweisungen hat.

Damit der Compiler `TmoLanguage` überhaupt kompiliert, muss ihm jetzt noch das Objekt wenigstens einmal kurz sichtbar gemacht werden. Ansonsten wird es auch nicht über die RTTI Routinen des `CellTypeRegisters` registriert. Hierzu bedienen wir uns einer Hilfsfunktion `RegisterExplizit` des `CellObjektes` im `initalization` Bereich der Unit.

```pascal
initialization
  TmoLanguage.RegisterExplicit;
  
end.
```

